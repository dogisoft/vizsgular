import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  movie: string = "Taxi";
  subtitle: string = "Find movie in the official IMDb database";
  title: string = 'Movie App in Angular 6';
  movieTitle: string = 'Taxi';
  items: object;
  restItemsUrl = 'http://www.omdbapi.com/?apikey=90409a6c&t='+this.movieTitle;

  constructor(private http: HttpClient) {}

  ngOnInit() {
      this.items = undefined;
      this.getRestItems();
  }

  // Read all
  getRestItems(): void {
    this.restItemsServiceGetRestItems()
      .subscribe(
        restItems => {
         if(restItems.Response == false){
              console.log(this.items)
          }
          else{
             this.items = restItems;
              console.log(this.items);
          }
        }
      )
  }
  // Rest Items Service: Read all REST Items
  restItemsServiceGetRestItems() {
    return this.http
      .get<any>(this.restItemsUrl)
      .pipe(map(data => data));
  }

    updateDom(event){
        this.items = undefined;
        this.movieTitle = this.movie;
        this.restItemsUrl = 'http://www.omdbapi.com/?apikey=90409a6c&t='+this.movieTitle;
        this.getRestItems();
    }
}
