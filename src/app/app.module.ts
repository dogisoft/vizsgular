import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BackendApiService } from './services/backend-api.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
      NgbModule.forRoot(), HttpClientModule, BrowserModule, FormsModule
  ],
  providers: [BackendApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
